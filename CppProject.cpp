
#include <iostream>
#include "Helpers.h"

int getValueFromUser()
{
    std::cout << "Enter an number: ";
    int x;
    std::cin >> x;
    return x;
}
int main()
{
    std::cout << "This program calculates" << 
        "the square of the sum of two numbers" << "\n";

    int a = getValueFromUser();
    int b = getValueFromUser();

    std::cout << "Result: " << "(" << a << " + " << 
        b << ")" << " Square = " << squareOfSum(a, b) << "\n";

    return 0;
}
